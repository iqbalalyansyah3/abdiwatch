<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Contact extends BaseController
{
    public function index()
    {
        //
    }

    public function submitForm()
    {
        $name = $this->request->getPost('name');
        $email = $this->request->getPost('email');
        $subject = $this->request->getPost('subject');
        $comments = $this->request->getPost('comments');

        // Simpan data ke database
        $contactModel = new \App\Models\ContactModel(); 
        $contactModel->save([
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'comments' => $comments
        ]);

        return 'Message sent successfully!'; 
    }
}