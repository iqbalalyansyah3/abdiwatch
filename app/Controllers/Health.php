<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Health extends BaseController
{
    public function index()
    {
        return view('abdihealth/health'); 
    }

    function lastdata(){
		ini_set('display_errors', 1);
		if (!isset($this->sess->user_type)) {
			redirect(base_url());
		}

		$error = "";
		$vehicle          = $_POST['device'];

		$nowdate          = date("Y-m-d");
		$nowday           = date("d");
		$nowmonth         = date("m");
		$nowyear          = date("Y");
		$lastday          = date("t");

		if ($vehicle == "") {
			$error .= "- Invalid Vehicle. Silahkan Pilih salah satu kendaraan! \n";
		}

		if ($error != "") {
			$callback['error'] = true;
			$callback['message'] = $error;

			echo json_encode($callback);
			return;
		}

		$user_id         = $this->sess->user_id;
		$user_level      = $this->sess->user_level;
		$user_company    = $this->sess->user_company;
		$user_subcompany = $this->sess->user_subcompany;
		$user_group      = $this->sess->user_group;
		$user_subgroup   = $this->sess->user_subgroup;
		$user_parent     = $this->sess->user_parent;
		$user_id_role    = $this->sess->user_id_role;
		$privilegecode   = $this->sess->user_id_role;
		$user_dblive 	   = $this->sess->user_dblive;
		$user_id_fix     = $user_id;

		$rows1           = array();
		$rows2           = array();
		$rows3           = array();
		// $type_data       = array("HRATE", "HRBP", "STEP", "TEMP");
		$type_data       = array("HRBP", "STEP", "TEMP");
		$datafix 				 = array();

		$this->db  = $this->load->database("webtracking_gps_abdiwatch_live", true);
		$title_history = "update";
		$curr_date     = date("d");
			for ($i=0; $i < sizeof($type_data); $i++) {
				if($vehicle != "0"){
					$device_ex 		 = explode("@", $vehicle);
					$this->dbtrip->where("gps_name", $device_ex[0]);
				}
				// $this->dbtrip->where("gps_ht_time >=", $stime);
				$this->dbtrip->where("gps_ht_code", $type_data[$i]);
				$this->dbtrip->order_by("gps_ht_time", "DESC");
				$this->dbtrip->limit(1);
				$q1   = $this->dbtrip->get("gps_health");
				$rows = $q1->result_array();

				if (sizeof($rows) > 0) {
					// if ($type_data[$i] == "HRATE") {
					$date_lastdata = date("d", strtotime($rows[0]['gps_ht_time']));
					if ($date_lastdata != $curr_date) {
						$title_history = "hist";
					}

					echo "<pre>";
					var_dump($date_lastdata.'-'.$curr_date.'-'.$title_history);die();
					echo "<pre>";

						array_push($datafix, array(
							"gps_name"        => $rows[0]['gps_name'],
							"gps_host"        => $rows[0]['gps_host'],
							"gps_type"        => $rows[0]['gps_type'],
							"gps_ht_code"     => $rows[0]['gps_ht_code'],
							"gps_ht_time"     => $rows[0]['gps_ht_time'],
							"gps_hour"        => date("H", strtotime($rows[0]['gps_ht_time'])),
							"gps_minute"      => date("i", strtotime($rows[0]['gps_ht_time'])),
							"gps_hr_rate"     => $rows[0]['gps_hr_rate'],
							"gps_bp_sys"      => $rows[0]['gps_bp_sys'],
							"gps_bp_dia"      => $rows[0]['gps_bp_dia'],
							"gps_temp"        => $rows[0]['gps_temp'],
							"gps_step"        => $rows[0]['gps_step'],
							"gps_oxy"         => $rows[0]['gps_oxy'],
							"gps_sleep"       => $rows[0]['gps_sleep'],
							"lastdata_status" => $title_history,
							"lastrefresh"      => date("Y-m-d H:i:s", strtotime("+1 hour")),
						));
				}
			}

			// echo "<pre>";
			// var_dump($datafix);die();
			// echo "<pre>";

		$this->dbtrip->close();


		if (count($rows) == 0) {
			$error .= "- No Data ! \n";
		}

		if ($error != "") {
			$callback['error'] = true;
			$callback['message'] = $error;

			echo json_encode($callback);
			return;
		}

		// echo "<pre>";
		// var_dump($rows);die();
		// echo "<pre>";

		$callback['data']           = $datafix;
		// $html = $this->load->view("newdashboard/swreport/vreport_result", $params, true);
		$callback['error'] = false;
		echo json_encode($callback);
	}
}