<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
// route dashboard //
$routes->get('dashboard', 'Dashboard::index');
// $routes->get('/', 'Dashboard::index');
$routes->get('tracker', 'Tracker::index');
$routes->get('health', 'Health::index');

// $routes->get('/contact', 'Contact::index');
$routes->post('/contact/submit', 'Contact::submitForm');

// Route Login //
$routes->get('/', 'Dashboard::index');
// $routes->get('/', 'SigninController::index');
$routes->get('/sign', 'SigninController::index');
$routes->get('/signup', 'SignupController::index');
$routes->match(['get', 'post'], 'SignupController/store', 'SignupController::store');
$routes->match(['get', 'post'], 'SigninController/loginAuth', 'SigninController::loginAuth');
$routes->get('/signin', 'SigninController::index');
$routes->get('/logout', 'SigninController::logout');
$routes->get('/profile', 'ProfileController::index',['filter' => 'authGuard']);