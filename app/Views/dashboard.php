<head>
    <meta charset="utf-8" />
    <title>Abdiwatch - Smartwatch</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=" " />
    <meta name="keywords" content="" />
    <meta content="Themesdesign" name="author" />

    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Bootstrap css -->
    <link rel="stylesheet" href="/assets/template/css/bootstrap.min.css" type="text/css" id="bootstrap-style" />

    <!-- Material Icon Css -->
    <link rel="stylesheet" href="/assets/template/css/materialdesignicons.min.css" type="text/css" />

    <!-- Unicon Css -->
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />

    <!-- Swiper Css -->
    <link rel="stylesheet" href="/assets/template/css/tiny-slider.css" type="text/css" />
    <link rel="stylesheet" href="/assets/template/css/swiper.min.css" type="text/css" />

    <!-- Custom Css -->
    <link rel="stylesheet" href="/assets/template/css/style.min.css" type="text/css" />

    <!-- colors -->
    <link href="/assets/template/css/colors/default.css" rel="stylesheet" type="text/css" id="color-opt" />

    <style>
    #clock-container {
        border: 1px solid #ccc;
        padding: 5px;
        border-radius: 5px;
    }
    </style>

</head>

<body data-bs-spy="scroll" data-bs-target="#navbarCollapse">
    <!-- light-dark mode button -->
    <a href="javascript: void(0);" id="mode" class="mode-btn text-white rounded-end" onclick="toggleBtn()">
        <i class="uil uil-brightness mode-dark mx-auto"></i>
        <i class="uil uil-moon bx-spin mode-light"></i>
    </a>
    <!-- START  NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-light" id="navbar">
        <div class="container-fluid">

            <!-- LOGO -->
            <a class="navbar-brand logo text-uppercase" href="index-1.html">
                <img src="/assets/login/images/abditrack-logo.png" class="logo-light" alt="" height="70">
                <img src="/assets/login/images/abditrack-logo.png" class="logo-dark" alt="" height="70">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="mdi mdi-menu"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ms-auto" id="navbar-navlist">
                    <li class="nav-item">
                        <a class="nav-link" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#service">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tracker">Tracker</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="health">Health Checking</a>
                    </li>
                </ul>
                <div id="clock-container">
                    <div id="clock"></div>
                </div>
                <div class="ms-auto">
                    <?php 
                    $session = session();
                    if ($session->has('name')) {
                        echo '<a href="/logout" class="btn bg-gradiant">Logout ('.$session->get('name').')</a>';
                    } else {
                        echo '<a href="/sign" class="btn bg-gradiant">Login</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- home section -->
    <section class="home bg-light" id="home">
        <!-- start container -->
        <div class="container">
            <!-- start row -->
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <img src="/assets/login/images/abditrack-logo.png" alt="" class="img-fluid mb-4 smallphone-image"
                        height="10">
                    <h1 class="display-5 fw-bold">Abditrack Wearable Device</h1>
                    <p class="mt-4 text-muted">Abditrack Wearable Device is the latest solution to your needs
                        your wearable device. With an elegant design and advanced features, Abditrack delivers
                        unmatched experience in the use of wearable devices. Enjoy comfort and
                        intelligence on your finger or wrist with Abditrack.</p>
                </div>

                <div class="col-lg-5 offset-md-1 ">
                    <img src="/assets/template/images/ring-&-smartwatch.png" alt="" class="img-fluid">
                    <div class="up-botton-text d-flex align-items-center d-none d-md-block">
                        <h5>Abditrack Wearable Device</h5>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->

        <div class="background-line"></div>
    </section>
    <!-- end home section -->
    <!-- service section -->
    <section class="section service bg-light" id="service">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="title text-center mb-5">
                        <h6 class="mb-0 fw-bold text-primary">Smartwatch Feature</h6>
                        <h2 class="f-40">"Stay Ahead with Our Smartwatch Feature!</h2>
                        <div class="border-phone">
                            <p class="text-muted">Harnessing the latest technology, our smartwatch feature is designed
                                to empower you in every aspect of your life. Seamlessly integrated into your daily
                                routine, it offers unparalleled convenience and efficiency.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-between">
                <div class="col-lg-4">
                    <div class="service-box text-center">
                        <div class="service-icon p-4"
                            style="background-image: url(./images/service/bomb.png); background-repeat: no-repeat; background-position: center;">
                            <i class="mdi mdi-security text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <a href="#">
                                <h5 class="fw-bold">HEALTH MEASUREMENT</h5>
                            </a>
                            <p class="text-muted">Our Health Measurement service focuses on evaluating cardiovascular
                                wellness. We provide comprehensive assessments to gauge various aspects of your
                                cardiovascular health, including heart rate, blood pressure, cholesterol levels, and
                                more. By monitoring these vital indicators, we help you understand your current health
                                status and identify areas for improvement. Whether you're proactively managing your
                                health or addressing specific concerns, our dedicated team is here to support you on
                                your journey towards optimal wellness.</p>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pt-4 pt-lg-0">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-4"
                            style="background-image: url(./images/service/bomb.png); background-repeat: no-repeat; background-position: center;">
                            <i class="mdi mdi-lightbulb-on text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <a href="#">
                                <h5 class="fw-bold">TRACKING</h5>
                            </a>
                            <p class="text-muted">Tracking your packages or shipments is an essential part of ensuring
                                they reach their destination safely and on time. With modern tracking systems, you can
                                monitor the progress of your deliveries every step of the way. Whether it's a small
                                parcel or a large shipment, tracking allows you to stay informed about the whereabouts
                                of your items.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
    <!-- start features -->
    <div class="section features" id="features">
        <!-- start container -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="title text-center mb-5">
                        <h6 class="mb-0 fw-bold text-primary">AppTech Features</h6>
                        <h2 class="f-40">Features for our app </h2>
                        <p class="text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor eos
                            inventore omnis aliquid rerum alias molestias.</p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">


                <div class="col-lg-12">
                    <ul class="nav nav-pills justify-content-center" id="pills-tab" role="tablist">
                        <li class="nav-item mb-3" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                                aria-selected="true">Top Features</button>
                        </li>
                    </ul>
                    <div class="tab-content mt-5" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                            aria-labelledby="pills-home-tab">
                            <div class="row align-items-center">
                                <div class="col-lg-4 order-2 order-lg-first">
                                    <div class="features-item text-start text-lg-end">
                                        <div class="icon avatar-sm text-center ms-lg-auto rounded-circle">
                                            <i class="mdi mdi-message-alert-outline f-24"></i>
                                        </div>
                                        <div class="content mt-3">
                                            <h5>Tracking</h5>
                                            <p class="text-muted">Tracking your packages or shipments is an essential
                                                part of ensuring
                                                they reach their destination safely and on time. With modern tracking
                                                systems, you can
                                                monitor the progress of your deliveries every step of the way. Whether
                                                it's a small
                                                parcel or a large shipment, tracking allows you to stay informed about
                                                the whereabouts
                                                of your items.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <img src="/assets/template/images/ring-&-smartwatch.png" alt="" class="img-fluid">
                                </div>
                                <div class="col-lg-4 order-last">
                                    <div class="features-item">
                                        <div class="icon avatar-sm text-center rounded-circle">
                                            <i class="mdi mdi-lifebuoy f-24"></i>
                                        </div>
                                        <div class="content mt-3">
                                            <h5>HEALTH MEASUREMENT</h5>
                                            <p>Our Health Measurement service focuses on evaluating
                                                cardiovascular
                                                wellness. We provide comprehensive assessments to gauge various aspects
                                                of your
                                                cardiovascular health, including heart rate, blood pressure, cholesterol
                                                levels, and
                                                more. By monitoring these vital indicators, we help you understand your
                                                current health
                                                status and identify areas for improvement. Whether you're proactively
                                                managing your
                                                health or addressing specific concerns, our dedicated team is here to
                                                support you on
                                                your journey towards optimal wellness.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                            aria-labelledby="pills-profile-tab">

                            <div class="row align-items-center">
                                <div class="col-lg-6">
                                    <img src="images/features/phone2.png" alt="" class="img-fluid">
                                </div>

                                <div class="col-lg-6">
                                    <h2 class="mb-4">Smart Features</h2>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <h6 class="mt-1">Fast Messaging</h6>
                                                        <p class="text-muted">Soluta velit sint, esse quis tempora
                                                            impedit corrupti in recusandae tenetur dignissimos
                                                            voluptates..</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <h6 class="mt-1">User Freindly</h6>
                                                        <p class="text-muted">Amet repudiandae illo quasi enim iusto
                                                            corporis ratione? Laudantium reprehenderit sint provident.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <h6 class="mt-1">Minimal Interface</h6>
                                                        <p class="text-muted">Repellat ad in autem, odio quos ex eum
                                                            recusandae cupiditate assumenda nihil incidunt dolorem qui
                                                            soluta.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <h6 class="mt-1">Notification</h6>
                                                        <p class="text-muted">Ipsam nisi quam velit maxime corrupti ut
                                                            quos, ad eum laudantium voluptatibus, facilis numquam
                                                            repellendus.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- end features -->

    <!-- cta section -->
    <section class="section cta" id="cta">
        <div class="bg-overlay-gradiant"></div>
        <!-- start container -->
        <div class="container position-relative">
            <div class="row">
                <div class="col-lg-6">
                    <div class="py-5">
                        <h1 class="display-4 text-white">Build Your ideal workspace today.</h1>
                        <p class="text-white-50 mt-3 f-18">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                            Iure distinctio vero facilis numquam sapiente! Eaque inventore eveniet repellendus quod
                            maiores nulla.</p>
                        <div class="d-flex mt-4 ">
                            <div class="app-store">
                                <a href=""><img src="images/img-appstore.png" alt="" class="img-fluid"></a>
                            </div>
                            <div class="googleplay">
                                <a href=""><img src="images/img-googleplay.png " alt="" class="img-fluid ms-3"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cta-phone-image">
                        <img src="images/cta-bg.png" alt="" class=" img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- end section -->

    <!-- contact section -->
    <section class="section contact overflow-hidden" id="contact">
        <!-- start container -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="title text-center mb-5">
                        <h6 class="mb-0 fw-bold text-primary">Contact Us</h6>
                        <h2 class="f-40">Connect with Us Today!</h2>
                        <p class="text-muted">Have a question, suggestion, or simply want to learn more about our
                            services? Don't hesitate to reach out! We're here to assist you every step of the way..</p>
                    </div>
                </div>
            </div>

            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="contact-box">
                        <div class="mb-4">
                            <h4 class=" fw-semibold mb-1">Need Support !</h4>
                            <p class="text-muted">Let's start a conversation and explore how we can help you achieve
                                your goals. Contact us now and let's make great things happen together!</p>
                        </div>

                        <div class="custom-form mt-4 ">
                            <form method="post" name="myForm" onsubmit="return validateForm()">
                                <p id="error-msg" style="opacity: 1;">
                                </p>
                                <div id="simple-msg"></div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input name="name" id="name" type="text" class="form-control contact-form"
                                                placeholder="Your name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input name="email" id="email" type="email"
                                                class="form-control contact-form" placeholder="Your email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group mt-2">
                                            <input type="text" class="form-control contact-form" id="subject"
                                                placeholder="Your Subject..">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group mt-2">
                                            <textarea name="comments" id="comments" rows="4"
                                                class="form-control contact-form h-auto"
                                                placeholder="Your message..."></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-2">
                                    <div class="col-lg-12 d-grid">
                                        <input type="submit" id="submit" name="send"
                                            class="submitBnt btn btn-rounded btn-primary" value="Send Message">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="m-5">
                        <div class="position-relative">
                            <div class="contact-map">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.023830898713!2d106.95736967475088!3d-6.260591093727971!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698ded2bd95ab7%3A0x1d2ec9d949e44d71!2sPT.%20Abditrack%20Inovasi%20Indonesia!5e0!3m2!1sen!2sid!4v1709544043994!5m2!1sen!2sid"
                                    width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                                    referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                            <div class="map-shape"></div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row mt-4">
                <div class="col-md-4">
                    <div class="d-flex align-items-center">
                        <div class="flex-shrink-0">
                            <i class="mdi mdi-google-maps f-50 text-primary"></i>
                        </div>
                        <div class="flex-grow-1 ms-3">
                            <h5 class="mb-1">Location</h5>
                            <p class="f-14 mb-0 text-muted">Jl. Cikunir Raya No.85, RT.004/RW.001, Jaka Mulya, Kec.
                                Bekasi Sel., Kota Bks, Jawa Barat 17146</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="d-flex align-items-center mt-4 mt-lg-0">
                        <div class="flex-shrink-0">
                            <i class="mdi mdi-email f-50 text-primary"></i>
                        </div>
                        <div class="flex-grow-1 ms-3">
                            <h5 class="mb-1">Email</h5>
                            <p class="f-14 mb-0 text-muted">Email: cs@abditrack.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="d-flex align-items-center mt-4 mt-lg-0">
                        <div class="flex-shrink-0">
                            <i class="mdi mdi-phone f-50 text-primary"></i>
                        </div>
                        <div class="flex-grow-1 ms-3">
                            <h5 class="mb-1">Whatapps</h5>
                            <p class="f-14 mb-0 text-muted">+62 811-5019-528 Cs Abditrack</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- end section -->
    <!-- footer section -->
    <section class=" section footer bg-dark overflow-hidden">
        <!-- container -->
        <div class="container">
            <div class="row ">
                <div class="col-lg-4">
                    <a class="navbar-brand logo text-uppercase" href="index-1.html">
                        <img src="/assets/login/images/abditrack-logo.png" class="logo-light" alt="" height="90">
                    </a>
                    <p class="text-white-50 mt-2 mb-0">Abditrack Wearable Device is the latest solution to your needs
                        your wearable device. With an elegant design and advanced features, Abditrack delivers
                        unmatched experience in the use of wearable devices. Enjoy comfort and
                        intelligence on your finger or wrist with Abditrack.</p>
                    <div class="footer-icon mt-4">
                        <div class=" d-flex align-items-center">
                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="WhatsApp">
                                <i class="mdi mdi-whatsapp f-24 align-middle text-success"></i>
                            </a>
                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="Gmail">
                                <i class="mdi mdi-email f-24 align-middle text-danger"></i>
                            </a>
                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="Instagram">
                                <i class="mdi mdi-instagram f-24 align-middle text-primary"></i>
                            </a>

                        </div>

                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="text-start mt-4 mt-lg-0">
                        <h5 class="text-white fw-bold">Follow Us Our Social Media</h5>
                        <ul class="footer-item list-unstyled footer-link mt-3">
                            <li>
                                <i class="mdi mdi-instagram f-23 text-primary"></i>
                                <p class="f-18 mb-0 text-muted">abditrackinovasi</p>
                            </li>
                        </ul>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 ">
                    <div class="text-start">
                        <h5 class="text-white fw-bold">Customer Service</h5>
                        <ul class="footer-item list-unstyled footer-link mt-3">
                            <i class="mdi mdi-email f-24 align-middle text-danger"></i>
                            <p class="f-19 mb-0 text-muted">Email: cs@abditrack.com</p>
                            <i class="mdi mdi-whatsapp f-24 align-middle text-success"></i>
                            <p class="f-19 mb-0 text-muted">+62 811-5019-528
                                Cs Abditrack</p>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4">
                    <h5 class="text-white">Avaible Now Apps</h5>
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>

    <section class="bottom-footer py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <p class="mb-0 text-center text-muted">©
                        <script>
                        document.write(new Date().getFullYear())
                        </script> Provided by IT Dept by <a href="https://www.abditrack.com/" target="_blank"
                            class="text-muted">PT. Abditrack
                            Inovasi Indonesia</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- end footer -->


    <!-- Style switcher -->
    <div class="style-switcher" id="style-switcher" onclick="toggleSwitcher()" style="left: -189px;">
        <div>
            <h6>Select your color</h6>
            <ul class="pattern list-unstyled mb-0">
                <li>
                    <a class="color1" href="javascript: void(0);" onclick="setColor('default')"></a>
                </li>
                <li>
                    <a class="color2" href="javascript: void(0);" onclick="setColor('blue')"></a>
                </li>
                <li>
                    <a class="color3" href="javascript: void(0);" onclick="setColor('warning')"></a>
                </li>
            </ul>
        </div>
        <div class="bottom">
            <a href="javascript: void(0);" class="settings rounded-end"><i class="uil uil-setting text-white"
                    style="display: inline; line-height: 46px;"></i></a>
        </div>
    </div>
    <!-- end switcher-->

    <!--Bootstrap Js-->
    <script src="/assets/template/js/bootstrap.bundle.min.js"></script>

    <!-- Slider Js -->
    <script src="/assets/template/js/tiny-slider.js"></script>
    <script src="/assets/template/js/swiper.min.js"></script>

    <!-- <script src="js/smooth-scroll.polyfills.min.js"></script> -->

    <!-- counter -->
    <!-- <script src="js/counter.init.js"></script> -->

    <!-- App Js -->
    <script src="/assets/template/js/app.js"></script>

    <script>
    function updateClock() {
        var now = new Date();
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var seconds = now.getSeconds();

        // Pad single digit minutes and seconds with leading zeros
        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;

        // Format the time as HH:MM:SS
        var timeString = hours + ':' + minutes + ':' + seconds;

        // Update the content of the clock element
        document.getElementById('clock').textContent = timeString;
    }

    // Call updateClock function every second
    setInterval(updateClock, 1000);

    // Call updateClock function once to initialize the clock
    updateClock();
    </script>

    <script>
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl);
    })
    </script>

</body>

</html>