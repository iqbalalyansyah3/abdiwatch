<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Custom Css -->
    <link rel="stylesheet" href="/assets/template/css/style.min.css" type="text/css" />
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="/assets/template/css/bootstrap.min.css" type="text/css" id="bootstrap-style" />
    <!-- Material Icon Css -->
    <link rel="stylesheet" href="/assets/template/css/materialdesignicons.min.css" type="text/css" />
    <!-- Unicon Css -->
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />
    <!-- Swiper Css -->
    <link rel="stylesheet" href="/assets/template/css/tiny-slider.css" type="text/css" />
    <link rel="stylesheet" href="/assets/template/css/swiper.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <title>Document</title>
    <style>
    /* Custom CSS untuk penataan view di bawah navbar */
    .option-section {
        background-color: #f8f9fa;
        padding: 90px;
        text-align: center;
    }

    .option-section h2 {
        margin-bottom: 50px;
    }

    .icon-section {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 40px;
    }

    .icon {
        margin: 0 10px;
        font-size: 36px;
    }
    </style>
</head>

<body>
    <!-- START NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-light" id="navbar">
        <div class="container-fluid">

            <!-- LOGO -->
            <a class="navbar-brand logo text-uppercase" href="index-1.html">
                <img src="/assets/template/images/logo-light.png" class="logo-light" alt="" height="30">
                <img src="/assets/template/images/logo-dark.png" class="logo-dark" alt="" height="30">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="mdi mdi-menu"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ms-auto" id="navbar-navlist">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tracker">Tracker</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="health">Health Checking</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                </ul>
                <div class="ms-auto">
                    <a href="login" class="btn bg-gradiant">Login</a>
                </div>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->

    <!-- Option Section -->
    <div class="option-section">
        <h2>ABDIWATCH DASHBOARD</h2>
        <div class="icon-section">
            <i class="fas fa-heartbeat icon" style="color: blue;"></i>
            <i class="fas fa-tint icon" style="color: red;"></i>
            <i class="fas fa-thermometer-half icon" style="color: green;"></i>
            <i class="fas fa-lungs icon" style="color: blue;"></i>
            <i class="fas fa-walking icon" style="color: green;"></i>
            <i class="fas fa-bed icon" style="color: yellow;"></i>
        </div>
    </div>

</body>

</html>