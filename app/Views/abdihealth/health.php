<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Custom Css -->
    <link rel="stylesheet" href="/assets/template/css/style.min.css" type="text/css" />
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="/assets/template/css/bootstrap.min.css" type="text/css" id="bootstrap-style" />
    <!-- Material Icon Css -->
    <link rel="stylesheet" href="/assets/template/css/materialdesignicons.min.css" type="text/css" />
    <!-- Unicon Css -->
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />
    <!-- Swiper Css -->
    <link rel="stylesheet" href="/assets/template/css/tiny-slider.css" type="text/css" />
    <link rel="stylesheet" href="/assets/template/css/swiper.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <title>Abdiwatch-Health</title>
    <style>
    .option-section {
        padding: 90px;
        text-align: center;
        background-size: cover;
        background-position: center;
    }


    select {
        margin: 50px;
    }

    .option-section h2 {
        margin-bottom: 50px;
    }

    .navbar {
        transition: all .5s ease;
        padding: 0;
        background-color: #fff !important;
    }

    .icon-section .icon {
        position: relative;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 5px;
        padding: 10px;
        margin: 0 5px;
        font-size: 200px;
    }

    .icon-heart {
        background-color: #00BFFF;
        color: red;
    }

    .icon-walk {
        color: red;
    }

    .text-green-bg {
        color: green;
        /* Warna ikon */
        background-color: lightgreen;
        /* Warna latar belakang */
    }

    .text-blue-bg {
        color: blue;
        /* Warna ikon */
        background-color: lightblue;
        /* Warna latar belakang */
    }

    .icon-blood-pressure {
        background-color: #F4A460;
        color: red;
    }

    .icon-oxgen {
        background-color: #90EE90;
        color: blue;
    }

    .icon-body-temp {
        background-color: red;
        color: blue;
    }

    .icon-number {
        position: absolute;
        top: 5px;
        right: 5px;
        background-color: #ffffff;
        border-radius: 50%;
        padding: 5px;
        font-size: 15px;
    }

    .icon-abbr {
        position: absolute;
        bottom: 5px;
        left: 5px;
        font-size: 15px;
    }
    </style>
</head>

<body>
    <!-- START NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-light" id="navbar">
        <div class="container-fluid">
            <!-- LOGO -->
            <a class="navbar-brand logo text-uppercase" href="index-1.html">
                <img src="/assets/template/images/logo-light.png" class="logo-light" alt="" height="30">
                <img src="/assets/template/images/logo-dark.png" class="logo-dark" alt="" height="30">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="mdi mdi-menu"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ms-auto" id="navbar-navlist">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tracker">Tracker</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="health">Health Checking</a>
                    </li>
                </ul>
                <div class="ms-auto">
                    <?php 
                    $session = session();
                    if ($session->has('name')) {
                        echo '<a href="logout" class="btn bg-gradiant">Logout ('.$session->get('name').')</a>';
                    } else {
                        echo '<a href="auth" class="btn bg-gradiant">Login</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- Option Section -->
    <div class="option-section">
        <h2 style="font-size: 20px;">ABDIWATCH DASHBOARD</h2>
        <select>
            <option>HTML</option>
            <option>HTML CSS3</option>
            <option active>HTML CSS3 SASS</option>
            <option>HTML CSS3 SASS LESS </option>
            <option>HTML CSS3 SASS LESS STYLUS</option>
            <option>JQUERY</option>
            <option>BOOTSTRAP</option>
            <option>MATERIAL</option>
            <option>REACT</option>
            <option>ANGULAR</option>
        </select>
        <div class="icon-section">
            <i class="fas fa-heartbeat icon icon-heart" title=" Heart Rate"><span class="icon-number">114</span><span
                    class="icon-abbr">HR</span></i>
            <i class="fas fa-tint icon icon-blood-pressure" title="Blood Pressure"><span
                    class="icon-number">155/7</span><span class="icon-abbr">BP</span></i>
            <i class="fas fa-thermometer-half icon icon-body-temp" title="Body Temperature"><span
                    class="icon-number">30</span><span class="icon-abbr">Temp</span></i>
            <i class="fas fa-tint icon icon-oxgen" title="Blood Oxygen"><span class="icon-number"
                    title="Blood Pressure">4</span><span class="icon-abbr">Oxygen</span></i>
            <i class="fas fa-walking icon text-green-bg" title="Step Counter"><span class="icon-number">5</span><span
                    class="icon-abbr">Walk</span></i>
            <i class="fas fa-bed icon text-blue-bg" title="Sleep Time"><span class="icon-number">6</span><span
                    class="icon-abbr">Bed</span></i>
        </div>
    </div>
    <!-- footer section -->
    <section class=" section footer bg-dark overflow-hidden">
        <div class="bg-arrow">

        </div>
        <!-- container -->
        <div class="container">
            <div class="row ">
                <div class="col-lg-4">
                    <a class="navbar-brand logo text-uppercase" href="index-1.html">
                        <img src="/assets/template/images/logo-footer.png" class="logo-light" alt="" height="30">
                    </a>
                    <p class="text-white-50 mt-2 mb-0">Smartwatches have revolutionized the way we manage our daily
                        activities, providing convenience and functionality right at our fingertips.</p>
                    <div class="footer-icon mt-4">
                        <div class=" d-flex align-items-center">
                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="WhatsApp">
                                <i class="mdi mdi-whatsapp f-24 align-middle text-success"></i>
                            </a>
                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="Gmail">
                                <i class="mdi mdi-email f-24 align-middle text-danger"></i>
                            </a>

                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="Linkedin">
                                <i class="mdi mdi-linkedin f-24 align-middle text-primary"></i>
                            </a>
                        </div>

                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="text-start mt-4 mt-lg-0">
                        <h5 class="text-white fw-bold">Follow Us Our Social Media</h5>
                        <ul class="footer-item list-unstyled footer-link mt-3">
                            <li>
                                <i class="mdi mdi-instagram f-23 text-primary"></i>
                                <p class="f-20 mb-0 text-muted text-white">abditrackinovasi</p>
                            </li>
                        </ul>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 ">
                    <div class="text-start">
                        <h5 class="text-white fw-bold">Policies</h5>
                        <ul class="footer-item list-unstyled footer-link mt-3">
                            <li><a href="">Security & Provciy</a></li>
                            <li><a href="">Marketplace</a></li>
                            <li><a href="">Term & Condition</a></li>
                            <li><a href="">Collection</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4">
                    <h5 class="text-white">Subscribe</h5>
                    <div class="input-group my-4">
                        <input type="text" class="form-control p-3" placeholder="subscribe" aria-label="subscribe"
                            aria-describedby="basic-addon2">
                        <a href="" class="input-group-text bg-primary text-white px-4" id="basic-addon2">Go</a>
                    </div>
                    <p class="mb-0 text-white-50">publishes will show up in your Subscriptions feed. You may also start.
                    </p>
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- end footer -->

    <!-- JavaScript untuk Bootstrap -->
    <script src="/assets/template/js/bootstrap.bundle.min.js"></script>
</body>

</html>