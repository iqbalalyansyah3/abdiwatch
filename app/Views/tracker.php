<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Custom Css -->
    <link rel="stylesheet" href="/assets/template/css/style.min.css" type="text/css" />
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="/assets/template/css/bootstrap.min.css" type="text/css" id="bootstrap-style" />
    <!-- Material Icon Css -->
    <link rel="stylesheet" href="/assets/template/css/materialdesignicons.min.css" type="text/css" />
    <!-- Unicon Css -->
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />
    <!-- Swiper Css -->
    <link rel="stylesheet" href="/assets/template/css/tiny-slider.css" type="text/css" />
    <link rel="stylesheet" href="/assets/template/css/swiper.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

    <title>Abdiwatch - Tracker</title>
    <style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #d7faf6;
    }

    .navbar {
        transition: all .5s ease;
        padding: 0;
        background-color: #fff !important;
    }

    .container {
        display: flex;
        justify-content: space-between;
        padding: 20px;
    }

    .options {
        width: 50%;
        padding: 70px;
        margin-right: 50px;
        margin-left: -90px;
        margin: 60px 0 30px 0;
        border: 1px solid #ccc;
        background-color: white;
    }

    .datetime-pickers {
        margin-bottom: 20px;
    }

    .checkboxes {
        margin-bottom: 20px;
    }

    .map {
        width: 100%;
        position: relative;
        margin-top: 70px;
        margin-left: 10px;
    }

    #map {
        width: 120%;
        height: 800px;
        padding: 10px;
        margin-left: 70px;
    }

    #playBtn,
    #stopBtn {
        border: 2px solid transparent;
        border-radius: 5px;
        background: none;
        cursor: pointer;
        padding: 5px;
        margin-right: 10px;

    }


    #playBtn .fas.fa-play {
        color: green;
    }

    /* Style for stop icon */
    #stopBtn .fas.fa-stop {
        color: red;
    }
    </style>
</head>

<body>
    <!-- START  NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-light" id="navbar">
        <div class="container-fluid">

            <!-- LOGO -->
            <a class="navbar-brand logo text-uppercase" href="index-1.html">
                <img src="/assets/template/images/logo-light.png" class="logo-light" alt="" height="30">
                <img src="/assets/template/images/logo-dark.png" class="logo-dark" alt="" height="30">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="mdi mdi-menu"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ms-auto" id="navbar-navlist">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tracker">Tracker</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="health">Health Checking</a>
                    </li>
                </ul>
                <div class="ms-auto">
                    <?php 
                    $session = session();
                    if ($session->has('name')) {
                        echo '<a href="logout" class="btn bg-gradiant">Logout ('.$session->get('name').')</a>';
                    } else {
                        echo '<a href="auth" class="btn bg-gradiant">Login</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <div class="container">
        <div class="options">
            <h3>Dashboard Smartwatch Tracker</h3>
            <div class="datetime-pickers">
                <label for="start">Start Date/Time:</label>
                <input type="datetime-local" id="start" name="start">
                <label for="end">End Date/Time:</label>
                <input type="datetime-local" id="end" name="end">
            </div>
            <div class="checkboxes">
                <input type="checkbox" id="showGPS" name="showGPS">
                <label for="showGPS">Show GPS</label><br>
                <input type="checkbox" id="showLBS" name="showLBS">
                <label for="showLBS">Show LBS</label><br>
                <input type="checkbox" id="showWiFi" name="showWiFi">
                <label for="showWiFi">Show WiFi</label><br>
                <!-- Play button -->
                <button id="playBtn">
                    <i class="fas fa-play"></i>
                </button>
                <!-- Stop button -->
                <button id="stopBtn">
                    <i class="fas fa-stop"></i>
                </button>
            </div>
        </div>
        <div class="map">
            <div id="map"></div>
        </div>
    </div>

    <!-- footer section -->
    <section class=" section footer bg-dark overflow-hidden">
        <div class="bg-arrow">

        </div>
        <!-- container -->
        <div class="container">
            <div class="row ">
                <div class="col-lg-4">
                    <a class="navbar-brand logo text-uppercase" href="index-1.html">
                        <img src="/assets/template/images/logo-footer.png" class="logo-light" alt="" height="30">
                    </a>
                    <p class="text-white-50 mt-2 mb-0">Smartwatches have revolutionized the way we manage our daily
                        activities, providing convenience and functionality right at our fingertips.</p>
                    <div class="footer-icon mt-4">
                        <div class=" d-flex align-items-center">
                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="WhatsApp">
                                <i class="mdi mdi-whatsapp f-24 align-middle text-success"></i>
                            </a>
                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="Gmail">
                                <i class="mdi mdi-email f-24 align-middle text-danger"></i>
                            </a>

                            <a href="" class="mx-2 avatar-sm text-center" data-bs-toggle="tooltip"
                                data-bs-placement="top" data-bs-title="Linkedin">
                                <i class="mdi mdi-linkedin f-24 align-middle text-primary"></i>
                            </a>
                        </div>

                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="text-start mt-4 mt-lg-0">
                        <h5 class="text-white fw-bold">Follow Us Our Social Media</h5>
                        <ul class="footer-item list-unstyled footer-link mt-3">
                            <li>
                                <i class="mdi mdi-instagram f-23 text-primary"></i>
                                <p class="f-20 mb-0 text-muted text-white">abditrackinovasi</p>
                            </li>
                        </ul>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 ">
                    <div class="text-start">
                        <h5 class="text-white fw-bold">Policies</h5>
                        <ul class="footer-item list-unstyled footer-link mt-3">
                            <li><a href="">Security & Provciy</a></li>
                            <li><a href="">Marketplace</a></li>
                            <li><a href="">Term & Condition</a></li>
                            <li><a href="">Collection</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- end footer -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgPzlDKOewmk4yCSmb-C4oEN10Y2211oA&callback=initMap"
        async defer></script>
    <script>
    // Initialize and add the map
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {
                lat: -6.2088,
                lng: 106.8456
            } // Default center to Jakarta
        });

        var coordinates = [{
                lat: -6.2088,
                lng: 106.8456
            },
            {
                lat: -6.2090,
                lng: 106.8460
            },
            {
                lat: -6.2092,
                lng: 106.8465
            }
        ];

        // Example marker
        var marker = new google.maps.Marker({
            position: {
                lat: -6.2088,
                lng: 106.8456
            },
            map: map,
            title: 'Jakarta'
        });

        // Example info window
        var infoWindow = new google.maps.InfoWindow({
            content: 'Name: Jakarta<br>Position Time: 2024-03-09<br>Latitude: -6.2088<br>Longitude: 106.8456<br>Course: 0<br>Speed: 0<br>Distance: 0'
        });

        marker.addListener('click', function() {
            infoWindow.open(map, marker);
        });

        // Create a polyline
        var polyline = new google.maps.Polyline({
            path: coordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        // Set polyline on the map
        polyline.setMap(map);
    }
    </script>