<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--favicon-->
    <link rel="icon" href="/assets/login/images/favicon-32x32.png" type="image/png" />
    <!--plugins-->
    <link href="/assets/login/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="/assets/login/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="/assets/login/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
    <!-- loader-->
    <link href="/assets/login/css/pace.min.css" rel="stylesheet" />
    <script src="/assets/login/js/pace.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="/assets/login/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/login/css/bootstrap-extended.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="/assets/login/css/app.css" rel="stylesheet">
    <link href="/assets/login/css/icons.css" rel="stylesheet">
    <title>Abditrack Wearable Device - Login</title>
</head>

<body class="">
    <!--wrapper-->
    <div class="wrapper">
        <div class="section-authentication-cover">
            <div class="">
                <div class="row g-0">
                    <div
                        class="col-12 col-xl-7 col-xxl-8 auth-cover-left align-items-center justify-content-center d-none d-xl-flex">
                        <div class="card shadow-none bg-transparent shadow-none rounded-0 mb-0">
                            <div class="card-body">
                                <img src="/assets/login/images/abditrack-logo.png"
                                    class="img-fluid auth-img-cover-login" width="650" alt="" />
                            </div>
                        </div>

                    </div>

                    <div class="col-12 col-xl-5 col-xxl-4 auth-cover-right align-items-center justify-content-center">
                        <div class="card rounded-0 m-3 shadow-none bg-transparent mb-0">
                            <div class="card-body p-sm-5">
                                <div class="">
                                    <div class="mb-3 text-center">
                                        <img src="/assets/login/images/abditrack-logo.png" width="300" alt="">
                                    </div>
                                    <div class="text-center mb-4">
                                        <h5 class="">Abditrack Wearable Device</h5>
                                        <p class="mb-0">Please log in to your account</p>
                                        <?php if(session()->getFlashdata('logout_msg')):?>
                                        <div class="alert alert-success">
                                            <?= session()->getFlashdata('logout_msg') ?>
                                        </div>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-body">
                                        <form class="row g-3"
                                            action="<?php echo base_url(); ?>/SigninController/loginAuth" method="post">
                                            <?php if(session()->getFlashdata('msg')):?>
                                            <div class="alert alert-warning">
                                                <?= session()->getFlashdata('msg') ?>
                                            </div>
                                            <?php endif;?>
                                            <div class="col-12">
                                                <label for="inputEmailAddress" class="form-label">Email</label>
                                                <input type="email" class="form-control" id="inputEmailAddress"
                                                    name="email" placeholder="jhon@example.com">
                                            </div>
                                            <div class="col-12">
                                                <label for="inputChoosePassword" class="form-label">Password</label>
                                                <div class="input-group" id="show_hide_password">
                                                    <input type="password" class="form-control border-end-0"
                                                        id="inputChoosePassword" name="password"
                                                        placeholder="Enter Password">
                                                    <a href="javascript:;" class="input-group-text bg-transparent"><i
                                                            class="bx bx-hide"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox"
                                                        id="flexSwitchCheckChecked" name="remember_me">
                                                    <label class="form-check-label"
                                                        for="flexSwitchCheckChecked">Remember Me</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid">
                                                    <button type="submit" class="btn btn-primary">Sign in</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="login-separater text-center mb-5">
                                        <span>OR SIGN IN WITH</span>
                                        <hr>
                                    </div>
                                    <div class="list-inline contacts-social text-center">
                                        <a href="javascript:;"
                                            class="list-inline-item bg-google text-white border-0 rounded-3"><i
                                                class="bx bxl-google"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!--end row-->
            </div>
        </div>
    </div>
    <!--end wrapper-->
    <!-- Bootstrap JS -->
    <script src="/assets/login/js/bootstrap.bundle.min.js"></script>
    <!--plugins-->
    <script src="/assets/login/js/jquery.min.js"></script>
    <script src="/assets/login/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="/assets/login/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="/assets/login/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <!--Password show & hide js -->
    <script>
    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("bx-hide");
                $('#show_hide_password i').removeClass("bx-show");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("bx-hide");
                $('#show_hide_password i').addClass("bx-show");
            }
        });
    });
    </script>
    <!--app JS-->
</body>

</html>