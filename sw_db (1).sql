-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2024 at 01:33 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sw_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `comments` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `comments`) VALUES
(1, 'iqbal', 'iqbalalyansyah3@gmail.com', 'halo', 'apa kabar sayang');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `delete_add`) VALUES
(1, 'abditrack', 'iqbal@abditrack.com', '$2y$10$Yh/o56xVRNCrvPrn76JCp.i.kLVz6NZxfujmb3fjJC2ccksSlnoO.', '2024-03-26 03:15:07', '2024-03-26 03:22:40');

-- --------------------------------------------------------

--
-- Table structure for table `webtracking_gps_abdiwatch_live`
--

CREATE TABLE `webtracking_gps_abdiwatch_live` (
  `id` int NOT NULL,
  `gps_name` varchar(255) DEFAULT NULL,
  `gps_host` int DEFAULT NULL,
  `gps_type` varchar(255) DEFAULT NULL,
  `gps_ht_code` int DEFAULT NULL,
  `gps_ht_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gps_hour` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gps_minute` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gps_hr_rate` int NOT NULL,
  `gps_bp_sys` int NOT NULL,
  `gps_bp_dia` int DEFAULT NULL,
  `gps_temp` int DEFAULT NULL,
  `gps_step` int NOT NULL,
  `gps_oxy` int NOT NULL,
  `gps_sleep` int NOT NULL,
  `lastdata_status` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastrefresh` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webtracking_gps_abdiwatch_live`
--
ALTER TABLE `webtracking_gps_abdiwatch_live`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `webtracking_gps_abdiwatch_live`
--
ALTER TABLE `webtracking_gps_abdiwatch_live`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
